#include <iostream>
#include <cstdio>

using namespace std;

int main(int argc, char** argv)
{
	int c0d03[56];
	FILE *fc0d0 = fopen("c0d0.txt", "r");
	for (int i = 0; i < 56; i++)
		fscanf(fc0d0, "%d", &c0d03[i]);
	fclose(fc0d0);

	int efunc[48];
	FILE *fefunc = fopen("efunc.txt", "r");
	for (int i = 0; i < 48; i++)
		fscanf(fefunc, "%d", &efunc[i]);
	fclose(fefunc);

	int ki_table[48];
	FILE *ftable = fopen("ki_table.txt", "r");
	for (int i = 0; i < 48; i++)
		fscanf(ftable, "%d", &ki_table[i]);
	fclose(ftable);

	FILE *ftablesi = fopen("si_table.txt", "r");
	int si_table[512];
	for (int i = 0; i < 512; i++)
		fscanf(ftablesi, "%d", &si_table[i]);
	fclose(ftablesi);

	FILE *fip1 = fopen("ip1.txt", "r");
	int ip1[64];
	for (int i = 0; i < 64; i++)
		fscanf(fip1, "%d", &ip1[i]);
	fclose(fip1);

	FILE *fp = fopen("perestanovka_p.txt", "r");
	int perestanovka_p[32];
	for (int i = 0; i < 32; i++)
		fscanf(fp, "%d", &perestanovka_p[i]);
	fclose(fp);

	FILE *fkey = fopen(argv[2], "rb");
	char kkk[8];
	fread(kkk, 1, 8, fkey);
	fclose(fkey);

	unsigned char key[64];
	//kkk -> key
	for (int i = 0; i < 8; i++)
	{
		unsigned char temp = 0x80;
		for (int j = 0; j < 8; j++)
		{
			if ((kkk[i] & temp) == 0)
				key[8 * i + j] = 0;
			else
				key[8 * i + j] = 1;
			temp = temp >> 1;
		}
	}
	//kkk -> key (end)

	//������ c0 � d0
	unsigned char c0[28], d0[28];
	int count = 0;
	unsigned char c0d02[56];
	for (int i = 0; i < 56; i++)
		c0d02[i] = key[c0d03[i] - 1];
	for (int i = 0; i < 28; i++)
	{
		c0[i] = c0d02[i];
		d0[i] = c0d02[i + 28];
	}
	//������ c0 � d0 (�����)

	//����� ����� c0 � d0
	unsigned char c0start[28], d0start[28];
	for (int i = 0; i < 28; i++)
	{
		c0start[i] = c0[i];
		d0start[i] = d0[i];
	}
	//����� ����� c0 � d0 (�����)


	FILE *f = fopen(argv[1], "rb");
	fseek(f, 0, SEEK_END);
	int file_len = ftell(f);
	int file_lenth = file_len / 8;
	if (file_len % 8 == 0)
		file_lenth--;
	rewind(f);

	FILE *f_out = fopen(argv[3], "wb");
	for (int file_lent = 0; file_lent <= file_lenth; file_lent++)
	{
		for (int i = 0; i < 28; i++)
		{
			c0[i] = c0start[i];
			d0[i] = d0start[i];
		}
		unsigned char aaa[8];
		if (file_len - file_lent * 8 < 8)
		{
			fread(aaa, 1, file_len - file_lent * 8, f);
			for (int i = file_len - file_lent * 8; i < 8; i++)
				aaa[i] = 0;
		}
		else
			fread(aaa, 1, 8, f);
		unsigned char bbb[64];

		//aaa -> bbb
		for (int i = 0; i < 8; i++)
		{
			unsigned char temp = 0x80;
			for (int j = 0; j < 8; j++)
			{
				if ((aaa[i] & temp) == 0)
					bbb[8 * i + j] = 0;
				else
					bbb[8 * i + j] = 1;
				temp = temp >> 1;
			}
		}
		//aaa -> bbb (end)

		//��������� ������������
		unsigned char bbbp[64];
		count = 0;
		for (int j = 57; j <= 63; j = j + 2)
			for (int i = j; i >= 0; i = i - 8)
			{
				bbbp[count] = bbb[i];
				count++;
			}
		for (int j = 56; j <= 62; j = j + 2)
			for (int i = j; i >= 0; i = i - 8)
			{
				bbbp[count] = bbb[i];
				count++;
			}
		//��������� ������������ (�����)

		//������ l0 � r0
		unsigned char l0[32], r0[32];
		for (int i = 0; i < 32; i++)
		{
			l0[i] = bbbp[i];
			r0[i] = bbbp[i + 32];
		}
		//������ l0 � r0 (�����)

		//����� ����������
		for (int q = 1; q <= 16; q++)
		{
			unsigned char cq[28], dq[28];
			//������ �q � dq
			for (int i = 0; i < 28; i++)
			{
				if (i != 27)
					cq[i] = c0[i + 1];
				else
					cq[i] = c0[0];
			}
			for (int i = 0; i < 28; i++)
				c0[i] = cq[i];
			for (int i = 0; i < 28; i++)
			{
				if (i != 27)
					dq[i] = d0[i + 1];
				else
					dq[i] = d0[0];
			}
			for (int i = 0; i < 28; i++)
				d0[i] = dq[i];
			if ((q != 1) && (q != 2) && (q != 9) && (q != 16))
			{
				for (int i = 0; i < 28; i++)
				{
					if (i != 27)
						cq[i] = c0[i + 1];
					else
						cq[i] = c0[0];
				}
				for (int i = 0; i < 28; i++)
					c0[i] = cq[i];
				for (int i = 0; i < 28; i++)
				{
					if (i != 27)
						dq[i] = d0[i + 1];
					else
						dq[i] = d0[0];
				}
				for (int i = 0; i < 28; i++)
					d0[i] = dq[i];
			}
			//������ �q � dq (�����)
			unsigned char kq[48];
			unsigned char cqdq[56];
			for (int i = 0; i < 28; i++)
			{
				cqdq[i] = cq[i];
				cqdq[i + 28] = dq[i];
			}

			//������ kq
			for (int i = 0; i < 48; i++)
				kq[i] = cqdq[ki_table[i] - 1];
			//������ kq (�����)
			
			//������ E(r i - 1)
			unsigned char erq[48];
			for (int i = 0; i < 48; i++)
				erq[i] = r0[efunc[i] - 1];
			//������ E(r i - 1) (�����)


			//erq + kq
			for (int i = 0; i < 48; i++)
				erq[i] = (erq[i] + kq[i]) % 2;
			//erq + kq (end)

			//������ si... � rq!
			unsigned char rqpre[32];
			for (int k = 0; k < 8; k++)
			{
				int str = 0;
				if (erq[k * 6] == 1)
					str += 2;
				if (erq[k * 6 + 5] == 1)
					str += 1;
				int col = 0;
				if (erq[k * 6 + 1] == 1)
					col += 8;
				if (erq[k * 6 + 2] == 1)
					col += 4;
				if (erq[k * 6 + 3] == 1)
					col += 2;
				if (erq[k * 6 + 4] == 1)
					col += 1;
				int si_cur = si_table[k * 64 + str * 16 + col];
				unsigned char v_rq[4];
				if (si_cur / 8 == 1)
					v_rq[0] = 1;
				else
					v_rq[0] = 0;
				si_cur = si_cur % 8;
				if (si_cur / 4 == 1)
					v_rq[1] = 1;
				else
					v_rq[1] = 0;
				si_cur = si_cur % 4;
				if (si_cur / 2 == 1)
					v_rq[2] = 1;
				else
					v_rq[2] = 0;
				si_cur = si_cur % 2;
				if (si_cur == 1)
					v_rq[3] = 1;
				else
					v_rq[3] = 0;
				for (int i = 0; i < 4; i++)
					rqpre[k * 4 + i] = v_rq[i];
			}
			//������ si... � rq! (�����)

			//������������ P
			unsigned char rq[32];
			for (int i = 0; i < 32; i++)
			{
				rq[i] = rqpre[perestanovka_p[i] - 1];
			}
			//������������ P (�����)

			//������� �������� lq � rq
			unsigned char rq_final[32];
			for (int i = 0; i < 32; i++)
				rq_final[i] = (l0[i] + rq[i]) % 2;
			for (int i = 0; i < 32; i++)
			{
				l0[i] = r0[i];
				r0[i] = rq_final[i];
			}
			//������� �������� lq � rq (�����)

		}
		//����� ���������� (�����)

		//�������� ������������
		unsigned char l0r0[64];
		for (int i = 0; i < 32; i++)
		{
			l0r0[i] = r0[i];
			l0r0[i + 32] = l0[i];
		}
		unsigned char output[64];
		for (int i = 0; i < 64; i++)
			output[i] = l0r0[ip1[i] - 1];
		//�������� ������������ (�����)

		//������� ������ � ascii
		unsigned char out[8];
		for (int i = 0; i < 8; i++)
			out[i] = 0;
		for (int i = 0; i < 8; i++)
		{
			unsigned char temp = 0x80;
			for (int j = 0; j < 8; j++)
			{
				if (output[i * 8 + j] == 1)
					out[i] = (out[i] | temp);
				temp = temp >> 1;
			}
		}
		//������� ������ � ascii (�����)

		fwrite(out, 1, 8, f_out);
	}
	
	fclose(f);
	fclose(f_out);
	return 0;
}